//
//  model.swift
//  test
//
//  Created by Alex Urev on 02/10/2019.
//  Copyright © 2019 Alex Urev. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

//var TodoItems: [[String: Any]] = [["Name": "Позвонить маме", "isCompleted": false], ["Name": "Сделать приложение", "isCompleted": true], ["Name": "Покушать", "isCompleted": false]]

var ToDoItems: [[String: Any]] {
    set{
        UserDefaults.standard.set(newValue, forKey: "TodoDataKey")
        UserDefaults.standard.synchronize()
    }
    get{
        if let array = UserDefaults.standard.array(forKey: "TodoDataKey") as? [[String: Any]] {
            return  array
        } else {
            return []
        }
    }
}

func addItem(nameItem: String, isCompleted: Bool = false){
    ToDoItems.append(["Name": nameItem, "isCompleted": false])
    setBadge()
}

func removeItem(at index: Int){
    ToDoItems.remove(at: index)
    setBadge()
}

func moveItem(fromIndex: Int, toIndex: Int) {
    let from = ToDoItems[fromIndex]
    ToDoItems.remove(at: fromIndex)
    ToDoItems.insert(from, at: toIndex)
}

func changeState(at item: Int) -> Bool{
    ToDoItems[item]["isCompleted"] = !(ToDoItems[item]["isCompleted"] as! Bool)
    setBadge()
    return ToDoItems[item]["isCompleted"] as! Bool
}

func requestForNotification() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.badge]) { (isEnable, error) in
        if isEnable {
            print("Согласие получено")
        }
    }
}

func setBadge() {
    var totalBadgeNumber = 0
    for item in ToDoItems{
        if (item["isCompleted"] as! Bool) == false {
            totalBadgeNumber += 1
        }
    }
    UIApplication.shared.applicationIconBadgeNumber = totalBadgeNumber
}

